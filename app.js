// "var" is used just because "variable" is a part of that simple modular approach.
// You should know, to get more: read about lexical declaration vs variables bubbling.
var APP = (function(app) {

	app = app || {};

	app.params = {
		credentions: {
			'$type': 'login',
			'username': 'user1234',
			'password': 'password1234'
		},
		api: 'wss://js-assignment.evolutiongaming.com/ws_api'
	};

	app.connect = function() {

		return new Promise(function(done, reject) {
			let channel = new WebSocket( this.params.api );
			channel.onopen = function() {
				console.log('The connection is established.');
				this.connection = channel;
				done();
			}.bind(this);
			channel.onmessage = function(msg) {
				this.notifier( JSON.parse(msg.data) );
			}.bind(this);
			channel.onerror = function(error) {
				console.error(error);
				reject(error);
			}.bind(this);
			channel.onclose = function(event) {
				if (event.wasClean) {
					console.log('The connection is finished.');
				} else {
					console.log('The connection is aborted');
				}
				this.unsubscribe();
				autoupdate__option.checked = false;
			}.bind(this);
		}.bind(this));
	};

	app.auth = function() {
		this.connection.send( JSON.stringify(this.params.credentions) );
	};
	app.ping = function() {
		this.connection.send(JSON.stringify({
			'$type': 'ping',
			seq: this.rand()
		}));
	};

	app.subscribe = function() {
		this.connection.send( JSON.stringify({
			'$type': 'subscribe_tables'
		}) );

		console.info('Subscribed to lobby updates');
	};

	app.unsubscribe = function() {
		this.connection.send( JSON.stringify({
			'$type': 'unsubscribe_tables'
		}) );
		
		console.info('Unsubscribed from lobby updates');
	};

	app.removeTable = function(id){
		this.connection.send( JSON.stringify({
			'$type': 'remove_table',
			id: id
		}) );
	};

	app.init = function() {

		this.isSubscribed = autoupdate__option.checked;

		this.connect().then(function() {
			this.auth();
			// ... Honestly, here we need to wait success authorization.
		
			this.ping();
			
			if ( this.isSubscribed ) { this.subscribe(); }
		
		}.bind(this));

		autoupdate__option.addEventListener('change', function(){
			APP.isSubscribed = this.checked;
			this.checked ? APP.subscribe() : APP.unsubscribe();
		}, false);

		lobby.addEventListener('click', function(e){
			if ( !e.target.classList.contains('table__remove') ) { return; }
			APP.removeTable( e.target.parentElement.id );


		}, false);
	};

	return app;
})(APP || {});