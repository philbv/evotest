Handlebars.registerHelper('seats', (items)=> {
	let out = '';
	let taken = items;
	for (let i=0, l=12; i<l; i++) {
		let classes = 'table__seat';
		classes += i+1 <= taken ? ' taken' : '';
		out += '<li class="' + classes + '">(°•°)</li>';
	}
	return out;
});