// "var" is used just because "variable" is a part of that simple modular approach.
// You should know, to get more: read about lexical declaration vs variables bubbling.
var APP = (function(app) {

	app = app || {};

	app.notifier = function(resp) {
		switch (resp['$type']) {
			case 'login_successful':
				console.info(resp['$type'], '/', resp.user_type);
			break;
			case 'login_failed':
				console.error(resp['$type']);
			break;
			case 'table_list':
				APP.updateUI( resp );
				console.info( 'All tables are here: ', resp.tables.length, 'ones' );
			break;
			case 'table_added':
				APP.updateUI__addTable( resp.table, resp.after_id );
				console.info( ' +  added table #', resp.after_id );
			break;
			case 'table_removed':
				APP.updateUI__removeTable( resp.id );
				console.info( ' -  removed table #', resp.id );
			break;
			case 'removal_failed':
				APP.restore( resp.id );
				console.info( ' •  not removed: #', parseInt(resp.id) );
			break;
			default:
				console.log('resp.$type: ', resp['$type']);
			break;
		}
	};

	return app;
})(APP || {});