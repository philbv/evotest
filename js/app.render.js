// "var" is used just because "variable" is a part of that simple modular approach.
// You should know, to get more: read about lexical declaration vs variables bubbling.
var APP = (function(app) {

	app = app || {};

	app.holder = document.createElement('div');

	app.templates = {};

	const templateEl = document.querySelector('#template-table');
	const templateHTML = templateEl.innerHTML;
	app.templates.table = Handlebars.compile( templateHTML );

	app.render = function(data) {
		try {
			return APP.templates.table(data);
		} catch (err) {
			console.error(err);
			return false;
		}
	};

	app.restore = function(id){
		const table = document.getElementById(id);
		table.classList.remove('removed');
	};

	app.updateUI = function(data) {
		const html = APP.render(data);
		lobby.innerHTML = html;
	};

	app.updateUI__removeTable = function(id, tableNode) {
		const table = tableNode || document.getElementById(id);
		if (!table) { return; }
		table.remove();
	};

	app.fromHTMLtoNODE = function(html) {
			APP.holder.innerHTML = html;
			
			const newTable = APP.holder.children[0];
			
			APP.holder.innerHTML = '';
			
			return newTable;
		};

	app.updateUI__addTable = function(table, after_id) {
		const prevSibling = document.getElementById(after_id);
		const newTableHTML = APP.render({tables: [table] });
		const newTable = APP.fromHTMLtoNODE( newTableHTML );
		
		if ( !prevSibling ) {
			if ( !lobby.children.length ) {
				lobby.appendChild( newTable );
			} else {
				lobby.insertBefore( newTable, lobby.children[0] );
			}
		} else {
			lobby.insertBefore( newTable, prevSibling );
		}
	};

	app.rand = function() { return Math.floor(Math.random()*1000000) };

	return app;
})(APP || {});